#include <iostream>

using namespace std;

/*
Tokens remaining 12

  How many tokens 1, 2 or 3? nim

Invalid input, try again.

Tokens remaining 12

  How many tokens 1, 2 or 3? 1
  Computer takes 3 tokens

Tokens remaining 8

  How many tokens 1, 2 or 3? 0

Must be a number between 1 and 3, try again.

Tokens remaining 8

  How many tokens 1, 2 or 3? 2
  Computer takes 2 tokens

Tokens remaining 4

  How many tokens 1, 2 or 3? 3
  Computer takes 1 token

Tokens remaining 0

  Computer wins!
*/
void showTokens(int tokens)
{
  cout << "Tokens remaining: " << tokens << endl;
}

int main()
{
  cout << "====\tWelcome to the game of Nim\t====\n\n\nThere are 12 tokens.\nYour goal is to take the last one.\nYou can take 1, 2, or 3 tokens at a time.\n\n";
  int tokens = 12;
  int ut = 0; // stores how many tokens the user takes
  int ct = 0; // stores how many tokens the computer takes

  do
  {
    cout << "How many tokens to take: ";
    cin >> ut;
    if (ut > 3 || ut < 1) // Must choose between 1 and 3 inclusive
    {
      cout << "Please choose a number between 1 and 3." << endl;
    }
    else
    {
      showTokens(tokens);
      ct = 4 - ut; // Computer always takes enough to make the total number of tokens a multiple of 4
      tokens -= 4;
      cout << "The computer takes " << ct << endl;
      showTokens(tokens);
    }
  } while (tokens > 0);

  cout << "The computer has taken the last token.\nYou have lost." << endl;
  return 0;
}