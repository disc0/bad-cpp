#include <iostream>

using namespace std;

bool *convertDecBin(unsigned short b)
{
    static bool array[15];
    
    for (int i = 15; i > -1; i--)
    {
        array[i] = b % 2;
        b /= 2;
    }

    return array;
}

void printBin(bool *binary)
{
    cout << "Binary  : ";
    short count = 0;
    for (int i = 0; i < 16; i++)
    {
        cout << binary[i];
        if (count == 3)
        {
            cout << " ";
            count = 0;
        } else 
        {
            count++;
        }
    }
}

int main()
{
    unsigned short userNum;

    cout << "Decimal : ";

    cin >> userNum;
    
    bool *binary = convertDecBin(userNum);

    printBin(binary);
    
    cout << endl;

    return 0;
}