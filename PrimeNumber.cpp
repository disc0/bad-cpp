#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// recursively check if n is prime
bool isPrime(int n, int d)
{
    if (n < 2)
        return 0;
    // n is only divisible by 1 and itself
    if (d == 1)
        return true;
    else
    {
        if (n % d == 0)
            return false;
        else
            return isPrime(n, d - 1);
    }
}

// prints numbers between 1 and 100 that are prime.
int main()
{
    int primes[25];
    int j = 0;
    for (int i = 0; i < 100; i++)
    {
        if (isPrime(i, i - 1))
        {
            primes[j] = i;
            j++;
        }
    }
    for (int i = 0; i < 25; i++)
    {
        cout << primes[i] << ", ";
    }
    cout << endl;

    return 0;
}