// Property Tax
// Actual value * .6
// $0.72 per $100 of assessment value
// ask for acutal value
// display assessment value
// display property tax

#include <iostream>

using namespace std;

int main()
{
    cout << "Please enter the actual value of your property: ";
    double realValue;
    cin >> realValue;
    double assessValue {realValue * 0.6};
    cout << "The assessment value of your property is $" << assessValue << endl;
    cout << "The tax for your property is $" << (assessValue / 100) * 0.72 << endl;
}