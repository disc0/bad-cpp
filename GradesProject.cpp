// AP Comp Sci GradesProject remake in c++

/*
 *  
 For this assignment, write a program that will input a set of grades and compute a weighted average.
The program should input 2 test scores (as ints), 3 quiz scores (also as ints) and a homework average (as a double).
The program should then output the test average, quiz average and final grade (as doubles).
The final grade should be a weighted average. Count the test average as 50% of the final grade, 
the quiz average as 30% of the final grade and the homework average as 20% of the final grade.

Sample Run:
Please enter your test grades.
Test 1: 89
Test 2: 86

Please enter your quiz grades.
Quiz 1: 84
Quiz 2: 84
Quiz 3: 83

Please enter your homework average.
Homework: 90.12

Test Average: 87.5
Quiz Average: 83.66666666666667
Final Grade: 86.874
 */

#include <iostream>

using namespace std;

int main()
{
    cout << "Please enter your test grades." << endl;
    int tests[2];
    cout << "Test 1: ";
    cin >> tests[0];
    cout << "Test 2: ";
    cin >> tests[1];

    cout << "Please enter your quiz grades." << endl;
    int quizes[3];
    cout << "Quiz 1: ";
    cin >> quizes[0];
    cout << "Quiz 2: ";
    cin >> quizes[1];
    cout << "Quiz 3: ";
    cin >> quizes[2];

    cout << "Please enter your homework average." << endl;
    double hwAvg;
    cout << "Homework: ";
    cin >> hwAvg;

    double testAvg = (tests[0] + tests[1]) / 2.0;
    double quizAvg = (quizes[0] + quizes[1] + quizes[2]) / 3.0;
    double finalGrade = (testAvg * .5) + (quizAvg * .3) + (hwAvg * .2);
    cout << "Test Average : " << testAvg << endl;
    cout << "Quiz Average : " << quizAvg << endl;
    cout << "Final Grade  : " << finalGrade << endl;
}