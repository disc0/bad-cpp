/*
 * Grade Calculator
 *
 * Write a program that will input a list of test scores in decimal from the keyboard. 
 * When the user enters -1, print the average of the scores as an integer.
 * 
 * What do you need to be careful about when using -1 to stop a loop?
 * 
 *     Sample Run:
 *         Enter the Scores:
 *         45.4
 *         100
 *         78.6
 *         -1
 *
 *         The average is: 74
 * 
 * 
 */

#include <iostream>

using namespace std;

int main()
{
    cout << "Enter the Scores: " << endl;

    double num = 0;
    double total = 0;
    int count = 0;
    while (num != -1)
    {
        cin >> num;
        if (num == -1)
        {
            break;
        }
        else
        {
            total += num;
            count++;
        }
    }

    // cout << "Total: " << total << endl;
    cout << "The average is: " << total / count << endl;
}